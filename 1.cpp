#include <iostream>
using namespace std;
 
typedef char DataType;
 
//二叉树结点定义 
struct node
{
   DataType data; //存放结点数据 
   struct node *lchild, *rchild ; //左右孩子指针 
};
typedef struct  node  BiTree;
typedef struct  node  *ptree;

// 栈
/*---------begin----------*/
struct stack_node
{
  ptree tnode;      //存放数据点
  int flag;         // 0为无孩子,1为有左孩子无右孩子
  struct stack_node *next;      //定义下一个结点
};

typedef struct stack_node *PNode;

PNode createNullStack( )
{
    PNode s = (PNode)malloc(sizeof(struct node));   //创建一个栈
//    if (NULL == s) {
//        printf("Out of Space!\n");
//        return NULL;
//    }
    s->next = NULL;     //下一项为空
    s->tnode = NULL;    //没有存数据
    s->flag = 0;        //表示无孩子
    return s;
}

int isNullStack(PNode s)
{//判断栈是否为空，若为空，返回值为1，否则返回值为0,若栈不存在，则返回-1
    if (NULL == s)
        return -1;
    else if (NULL == s->next)
        return 1;
    else
        return 0;
}
int push(PNode s ,ptree tnode)      //头插法插入
{//在栈中插入数据元素x，若插入不成功，返回0；插入成功返回值为1
    PNode p = (PNode)malloc(sizeof(struct node));   //创建一个栈结点
    if (NULL == p)      //判断是否插入成功
        return 0;
    p->tnode = tnode;      //链接结点
    p->flag = 0;            // 新入栈的结点没有孩子
    p->next = s->next;      //将新创建的结点p链接在s中（头插法）
    s->next = p;
    return 1;
}
ptree pop(PNode s)
{//弹栈并返回删除元素，若栈为空，则返回NULL
    if (NULL == s || NULL == s->next)       //判断是否栈空
        return NULL;
    PNode p = s->next;      //将p指向下一个结点
    ptree tnode = p->tnode;    //记录节点数据
    s->next = s->next->next;
    free(p);
    p = NULL;
    return tnode;
}
/*---------end----------*/

//函数可直接使用，功能：输出结点数据
void print(DataType d)
 {
   cout<<d<<" ";
 }

/*
函数名：createBiTree
函数功能：创建二叉树，并返回二叉树的根结点指针 
参数：无 
返回值：二叉树的根结点指针 
*/ 
BiTree *createBiTree() {
//请在此处填写代码，完成创建二叉树并返回二叉树根结点指针的功能    
/*-------begin--------*/
    PNode s = createNullStack();        //创建一个栈结构
    ptree tree = NULL;                  //创建一个空树
    ptree root = NULL;
    int i = 0;
    DataType str[80] = {};      //记录输入数据
    DataType data;
    cin >> str;
    while ((data = str[i++]) != '\0')       //判断输入是否为空
    {
        if(data == '#') {
            root = NULL;        //如果输入进来的是“#”，则结点为空
        } else {
            root = new BiTree;      //开辟一个新的二叉树结点
            if (NULL == tree)       //如果整棵树是空的，则将结点赋为根节点
                tree = root;
            root->data = data;      //存入节点数据
            root->lchild = NULL;       //结点左孩子为空
            root->rchild = NULL;        //结点右孩子为空
        }
        if(!isNullStack(s))         //如果栈不为空
        {
            switch(s->next->flag)   //判断栈结点的flag
            {
                case 0:             //如果是0，代表着结点没有左孩子和右孩子
                    s->next->tnode->lchild = root;      //将root给结点左孩子
                    s->next->flag++;            //flag加一
                    break;
                case 1:     //如果是1，代表着结点有左孩子但是没有右孩子
                    s->next->tnode->rchild = root;      //将root结点赋值给右孩子
                    pop(s);
                    // 若左右孩子均确定, 则该节点确立, 弹出构建栈
                    break;
            }
        }
        // 如果新建结点不为空, 那么将该结点压入构建栈中
        if (NULL != root)
            push (s, root);     //栈为空结点不为空，则s为栈底
    }
    return tree;        //操作完成之后返回树
}

/*
函数名：preOrder
函数功能：先根遍历二叉树 
参数：二叉树根结点指针 
返回值：无 
*/
void preOrder(BiTree *T) 
{
//请在此处填写代码，完成先根遍历二叉树功能    DLR
/*-------begin--------*/
//    if (T == NULL)
//        return;
//    ptree c = T;
    PNode s = createNullStack();
    do {
        while (T != NULL)   //如果树不为空
        {
            print(T->data);     //树的根节点输出（先根遍历）
            push(s, T);         //将T结点入栈到s中
            T = T->lchild;      //T结点转到左孩子
        }
        while ((T == NULL) && (!isNullStack(s)))        //如果T树空了，并且s树没空
        {
            T = s->next->tnode->rchild;         //T结点为s的右孩子
            pop(s);            //出栈s
        }
    } while (T != NULL);        //直到T空
/*-------end--------*/  
}
 
/*
函数名： inOrder
函数功能：中根遍历二叉树 
参数：二叉树根结点指针 
返回值：无 
*/  
void inOrder(BiTree *T) 
{
    //请在此处填写代码，完成中根遍历二叉树功能    LDR
/*-------begin--------*/
//    if (T == NULL)
//        return;
//    ptree c = T;
    PNode s = createNullStack();        //创建一个栈
    do {
        while (T != NULL)       //如果栈没空
        {
            push(s, T);         //入结点T到s中
            T = T->lchild;      //进入左孩子
        }
        while ((T == NULL) && (!isNullStack(s)))        //如果T空了，s没空
        {
            T = s->next->tnode->rchild;     //T为右孩子
            print(pop(s)->data);        //出s的值
        }
    } while (T != NULL);
/*-------end--------*/  
}

/*
函数名：postOrder
函数功能：后根遍历二叉树 
参数：二叉树根结点指针 
返回值：无 
*/  
 void postOrder(BiTree *T)  
    {  
      //请在此处填写代码，完成后根遍历二叉树功能    LRD
/*-------begin--------*/
//    if (T == NULL)
//        return;
//    ptree c = T;
    PNode s = createNullStack();        //创建一个新的栈
    do {
        while (T != NULL)       //若栈没空
        {
            push(s, T);         //直接入栈
            if (s->next->flag >= 1)         // 如果左孩子访问过了, 那么跳过左孩子的访问
                break;
            T = T->lchild;      // 访问左孩子
            s->next->flag++;    //表明有左孩子
        }
        while (!isNullStack(s))
        {
            if (1 == s->next->flag)
            {
                // 如果是从左孩子进来的, 那么访问右孩子
                // 访问右孩子
                T = s->next->tnode->rchild;
                s->next->flag++;
                break;
            } else
                {  // 如果已经访问完了左右孩子
                // 访问该结点
                print(s->next->tnode->data);
                pop(s);
            }
        }
    } while (!isNullStack(s));

/*-------end--------*/
    }



int main(void)
{
    BiTree    *T;  
     T =  createBiTree(   ); //调用创建二叉树功能，得到二叉树的根结点指针
 
	 
    preOrder(  T );//调用先根遍历二叉树，按先根遍历顺序输出二叉树结点功能 
    cout<<endl; //换行
	inOrder(T);//调用中根遍历二叉树，按中根遍历顺序输出二叉树结点功能 
	cout<<endl;
    postOrder(T);//调用后根遍历二叉树，按后根遍历顺序输出二叉树结点功能 
   
   return 1;
}  
