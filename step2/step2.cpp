#include <cstddef>
#include <ios>
#include <iostream>
using namespace std;

//假设我们用一棵二叉树存储你的家族族谱：
//对于任意一个结点来说，左孩子代表长子或长女，右孩子代表兄弟或姐妹。
//结点存储的是每个人的代号。
//本关任务要求：输入某人的代号，输出此人的所有兄弟姐妹。
// in put A
// out put E F G
typedef char DataType;
//二叉树结点定义 
//此处进行二叉树结构定义
/*-------begin------*/
typedef struct node Stuct;
typedef struct node  BiTree;
struct node{
    DataType info;
    Stuct* lchild,*rchild;
};
/*-----end---------*/



void print(DataType d)
 {
   cout<<d<<" ";
 }

//初始化二叉树的结点序列
char treeData[] ="ABC##D##E#F#G##"; 

/*
函数名：createBiTree
函数功能：读取treeData 数组中的字符序列进行二叉树创建二叉树，并返回二叉树的根结点指针 
参数：无 
返回值：二叉树的根结点指针 
*/ 
 

    //在此处填入代码
    /*----------begin-------------*/
  int i=0;
  Stuct* createBiTree() {
      Stuct *root;
      if (treeData[i] == '#') {
          root = NULL;
          i++;
      } else {
          root = new struct node;
          root->info = treeData[i++];
          root->lchild = createBiTree();
          root->rchild = createBiTree();
      }
      return root;
  }
    /*----------end-------------*/
 
void Sibling(BiTree *T ) 
{
   //在此处填入代码,输出结点T的所有兄弟姐妹
    /*----------begin-------------*/
    if(T ==NULL) {
        return ;
    }
    else{
        printf("%c ",T->info);
        Sibling(T->rchild);
    }
    
    /*----------end-------------*/
}

/*
函数名：preOrder
函数功能：先根遍历二叉树 ，并找到值为ch的结点指针
参数：根结点指针 BiTree *T  ,需查找的结点值 ch 
返回值：无 
*/
 
   //在此处填入代码,利用先序遍历，找到结点值ch后调用函数sibling输出该结点的所有兄弟姐妹，以空格分界
    /*----------begin-------------*/
void preOrder(Stuct* T,char ch )
{
    if (T == NULL)
        return;
    if(T->info == ch)
    {
        Sibling(T->rchild);
    }
    preOrder(T->lchild,ch);
    preOrder(T->rchild,ch);
}
    
    /*----------end-------------*/
 


 
int main()
{  
    BiTree    *T;  
    T=  createBiTree(   ); //创建二叉树
 
	char ch ;
    cin>>ch; //输入某人的代号
    preOrder(T, ch); //调用函数输出ch的所有兄弟姐妹

}  
